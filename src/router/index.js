import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/headlines',
  },
  {
    path: '/headlines',
    name: 'Headlines',
    component: () => import('../views/Headlines.vue'),
  },
  {
    path: '/headline/:id',
    name: 'Headline',
    component: () => import('../views/HeadlineDetails.vue'),
  },
  {
    path: '/error',
    name: 'Error500',
    component: () => import('../views/Error500.vue'),
  },
  {
    path: '/error-404',
    name: 'Error404',
    component: () => import('../views/Error404.vue'),
  },
  {
    path: '*',
    name: 'CatchAll',
    component: () => import('../views/Error404.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
