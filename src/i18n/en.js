export default {
  unexpectedErrMsg: 'Unexpected error encountered. Please contact System Administrator.',
};
