import moment from 'moment';
import { DATE_FORMAT } from '@/constants';

const DateFilters = {
  /*
   * Converts an ISO date string to human-readable string.
   * @param  {String} isoDate
   *     - The ISO date string to format
   */
  isoFormatter(isoDate) {
    return moment(isoDate).format(DATE_FORMAT);
  },
};

export default DateFilters;
