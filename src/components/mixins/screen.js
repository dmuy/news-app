const defaultActionResult = {
  success: true,
  data: {},
};

export default {
  inject: ['$app'],
  data() {
    return {
      alerts: [],
    };
  },
  methods: {
    /**
     * Shows an alert on the screen.
     * @param {Object} alert
     *     - The alert to show. Must contain `type` and `message` keys.
     */
    __showAlert(alert) {
      this.__showAlerts([alert]);
    },
    /**
     * Shows multiple alerts on the screen.
     * @param {Array} alerts
     *     - The alerts to show. Each object in the array must contain `type` and `message` keys.
     */
    __showAlerts(alerts = []) {
      this.alerts.splice(0);
      alerts.forEach((alert) => {
        this.alerts.push(alert);
      });
    },
    /**
     * Executes a service method. This method is used for generic error handling in case
     * the service method returns an error.
     * @param  {Function} serviceMethod
     *     - The service method to be invoked. Must return a promise.
     * @param  {Object} actionParameters
     *     - Parameters to add as request headers for the service method to be invoked.
     * @returns a Promise which returns an ActionResult object.
     */
    __doAction(serviceMethod, actionParameters = {}) {
      this.$app.toggleLoadingFlag(true);
      return serviceMethod(actionParameters)
        .then((resp) => {
          const actionResult = { ...defaultActionResult };

          if (resp.status === 200) {
            actionResult.success = true;
            actionResult.data = resp.data;
          } else {
            actionResult.success = false;
            this.__showAlert({
              message: resp.message,
              type: 'error',
            });
          }

          this.$app.toggleLoadingFlag(false);
          return actionResult;
        })
        .catch((err) => {
          const actionResult = { ...defaultActionResult, success: false };

          if (this.$app.debugEnabled) {
            console.debug(err);
          }

          this.__showAlert({
            message: this.$vuetify.lang.t('$vuetify.unexpectedErrMsg'),
            type: 'error',
          });

          this.$app.toggleLoadingFlag(false);
          return actionResult;
        });
    },
  },
};
