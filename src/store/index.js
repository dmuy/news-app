import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const HASHER = {
  /*
  * Random hasher I found on stackoverflow.
  * https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
  * Decided to create a key by hashing an article's title and publishedAt.
  * Ideally, I shouldn't have to do this but newsapi.org doesn't provide
  * a unique ID for each article, and it would be too expensive to keep
  * querying from /top-headlines just to find a specific article.
  * @param  {String} key
  *     - The key to hash
  */
  hash(key) {
    let hash = 0;
    let i;
    let chr;
    if (key.length === 0) return hash;
    for (i = 0; i < key.length; i += 1) {
      chr = key.charCodeAt(i);
      // eslint-disable-next-line no-bitwise
      hash = ((hash << 5) - hash) + chr;
      // eslint-disable-next-line no-bitwise
      hash |= 0;
    }
    return `headline_${hash}`;
  },
};

export default new Vuex.Store({
  state: {
    headlines: {},
    visited: [],
  },
  mutations: {
    addHeadline(state, payload) {
      if (payload.key && payload.headline) {
        state.headlines[payload.key] = payload.headline;
      }
    },
    addVisited(state, key) {
      if (key && state.headlines[key]) {
        const idx = state.visited.indexOf(key);

        if (idx > -1) {
          state.visited.splice(idx, 1);
        }

        state.visited.unshift(key);
      }
    },
  },
  actions: {
    addHeadline({ commit }, headline) {
      let key = null;

      // sanity-check
      if (headline && headline.title && headline.publishedAt) {
        /*
         * weak key algorithm: assuming title and publishedAt are unique
         * for each headline, since I could not find a unique ID given by
         * newsapi.org.
         */
        key = HASHER.hash(`${headline.title} ${headline.publishedAt}`);
        commit('addHeadline', { key, headline });
      }

      return key;
    },
  },
  modules: {
  },
});
