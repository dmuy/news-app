import axios from 'axios';
import {
  API_TOKEN,
  URL_BASE,
  URL_TOP_HEADLINES,
  URL_SOURCES,
} from '@/constants';

const NEWS_AXIOS = axios.create({
  baseURL: URL_BASE,
});

const SUGGESTION_MAX_SIZE = 5;

const defaultParameters = {
  apiKey: API_TOKEN,
  country: 'us',
};

/*
 * This service is used to communicate with the API server.
 */
export default {
  /**
   * Retrieves the top headlines from the server.
   * @param  {Object} params
   *     - The parameters to be included in the request header
   */
  getTopHeadlines(params = {}) {
    const finalParams = Object.assign({}, defaultParameters, params);

    return NEWS_AXIOS.get(URL_TOP_HEADLINES, {
      params: finalParams,
    });
  },
  /**
   * Retrieves the sources from the server.
   * @param  {Object} params
   *     - The parameters to be included in the request header
   */
  getSources(params = {}) {
    const finalParams = Object.assign({}, defaultParameters, params);

    return NEWS_AXIOS.get(URL_SOURCES, {
      params: finalParams,
    });
  },
  /**
   * Retrieves the headlines that contains the given text.
   * @param  {String} keyword
   *     - The keyword to search for
   */
  searchHeadlines(keyword = '') {
    const finalParams = { ...defaultParameters, q: keyword, pageSize: SUGGESTION_MAX_SIZE };
    delete finalParams.country;

    return NEWS_AXIOS.get(URL_TOP_HEADLINES, {
      params: finalParams,
    });
  },
};
