// The API_TOKEN to use for authentication with newsapi.org
const API_TOKEN = '32c8b2e9ac4949888b19eb2775694859';

// newsapi.org endpoints
const URL_BASE = 'https://newsapi.org/v2';
const URL_TOP_HEADLINES = '/top-headlines';
const URL_SOURCES = '/sources';

// If set to false, debug logs will not be shown
const DEBUG_ENABLED = true;

// The default format to use for displaying dates
const DATE_FORMAT = 'MMMM Do YYYY, h:mm:ss a';

// The number of headlines to fetch per query
const DEFAULT_QUERY_SIZE = 20;

export {
  API_TOKEN,
  URL_BASE,
  URL_TOP_HEADLINES,
  URL_SOURCES,
  DEBUG_ENABLED,
  DATE_FORMAT,
  DEFAULT_QUERY_SIZE,
};
